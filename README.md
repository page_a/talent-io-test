## Test talent-io

### Part I - Algo
The solution I propose is to use hash, which has a far less important complexity. If I remember well, to access an element at indices N, it's only one operation O(1).
Overall the algorythm complexity is O(n), this is because it's proportionnal to the number of item. First time we make a copy of the array as a hash with as key, the value of the array, and as value the quantity.
And then reiterate a second time to look for tuples.

The rake task is available in `lib/tasks/main.rake`

### Part II - Backend
I implemented everything asked. 

If you want to convert the amount 13,000 from USD to EUR, based on the exchange rate on 19/03/2018 simply launch
`be rake fixerio:convert[13000,'USD','EUR','2018-03-19']` 
I used interactor to implement the wrapper around FixerIO api.
All the requested function are located in the model.

I populate the file `seeds.rb` to make everything smoother. -> `rails db:seed`

### Part IV - DataBase
Here is the SQL request : 

```
SELECT  "talent_advocates".* FROM "talent_advocates" INNER JOIN "candidates" ON "candidates"."talent_advocate_id" = "talent_advocates"."id" GROUP BY talent_advocate_id ORDER BY sum(job_offers_count) DESC
```

The issue is solved by using the rails counter cache. (job_offers_count)  I also implemented the solution in the project using scope.
