# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# Deal for computing by year, month, week
deal_one = Deal.create(amount: 13000, currency: 'USD', created_at: DateTime.parse('2010-01-03'))
deal_two = Deal.create(amount: 7000, currency: 'GBP', created_at: DateTime.parse('2010-02-16'))
deal_three = Deal.create(amount: 10000, currency: 'EUR', created_at: DateTime.parse('2010-03-27'))
deal_four = Deal.create(amount: 10000, currency: 'EUR', created_at: DateTime.parse('2009-01-27'))
deal_five = Deal.create(amount: 10000, currency: 'EUR', created_at: DateTime.parse('2009-01-29'))
deal_six = Deal.create(amount: 10000, currency: 'EUR', created_at: DateTime.parse('2009-01-24'))

# TalentAdvocates for database exercice
advocate_one = TalentAdvocate.create(name: 'John Doe')
advocate_two = TalentAdvocate.create(name: 'Jane Washington')
advocate_three = TalentAdvocate.create(name: 'Bob Dylan')
advocate_four = TalentAdvocate.create(name: 'Roger Federer')
advocate_five = TalentAdvocate.create(name: 'Donald Trump')

# Candidates
candidate_one = advocate_one.candidates.create(name: 'Alex')
candidate_two = advocate_one.candidates.create(name: 'Florian')
candidate_three = advocate_one.candidates.create(name: 'Matthieu')
candidate_four = advocate_two.candidates.create(name: 'Emilien')


# Job offers
candidate_one.job_offers.create()
candidate_one.job_offers.create()
candidate_one.job_offers.create()
candidate_one.job_offers.create()
candidate_two.job_offers.create()
candidate_two.job_offers.create()
candidate_two.job_offers.create()
candidate_three.job_offers.create()
candidate_three.job_offers.create()
candidate_four.job_offers.create()
