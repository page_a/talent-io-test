class CreateCandidates < ActiveRecord::Migration[5.2]
  def change
    create_table :candidates do |t|
      t.string :name
      t.references :talent_advocate, foreign_key: true

      t.timestamps
    end
  end
end
