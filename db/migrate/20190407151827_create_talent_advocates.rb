class CreateTalentAdvocates < ActiveRecord::Migration[5.2]
  def change
    create_table :talent_advocates do |t|
      t.text :name
      
      t.timestamps
    end
  end
end
