class AddCounterCacheToCandidates < ActiveRecord::Migration[5.2]
  def self.up
    add_column :candidates, :job_offers_count, :integer, :default => 0
    Candidate.find_each do |c|
      c.update_attribute(:job_offers_count, c.job_offers.count)
    end
  end

  def self.down
    remove_column :candidates, :job_offers_count
  end
end
