namespace :fixerio do
    desc 'Convert amount from one currency to the other'
    task :convert, [:amount, :from, :to, :date] => :environment do |_t, args|
        context = ::Services::Fixerio::Convert.call(amount: args.amount, from: args.from,
                                          to: args.to, date: args.date)
        if context.success?
            puts context.converted_result
        else
            puts context.error
        end
    end
end
