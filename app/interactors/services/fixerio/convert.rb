require 'net/http'

module Services
    module Fixerio
        BASE_URL = 'http://data.fixer.io/api'
        class Convert
            include Interactor
            
            def call
                context.fail!(error: 'Missing parameters') if context.from.nil? || context.to.nil? || context.amount.nil? || context.date.nil?
                from = context.from
                to = context.to
                amount = context.amount.to_i
                date = context.date
                symbols = [context.from, context.to].join(',')

                response = response = HTTParty.get("#{BASE_URL}/#{date}?access_key=#{ENV['FIXER_API_KEY']}&symbol=#{symbols}")
                body = JSON.parse(response.body)
                if body.dig('success')
                    context.rates = response.dig('rates')
                    context.converted_result = (amount / context.rates.dig(context.from)) * context.rates.dig(context.to)
                else
                    context.fail!(error: body.dig('error', 'info'), status_code: body.dig('error', 'code'))
                end
            end
        end
    end
end