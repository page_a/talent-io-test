module Sum
    class Main
        include Interactor

        def call
            file = File.read(Rails.root.join('storage', 'tupple_sums', 'unordered_integers.json'))
            array = JSON.parse(file)
            # array = [-5, 13, 4, 9, -1, 13]
            tuples = couple_summing(array, 8765)
            pp tuples.values
        end

        def couple_summing(array, nbr)
            hash = {}
            solutions = {}
            i = 0
            while i < array.length do
                if hash.dig(array[i]) == nil
                    hash[array[i]] = 2
                else
                    hash[array[i]] += 2
                end
                i = i + 1
            end
            i = 0
            while i < array.length do
                good_number = nbr - array[i]
                result = hash.dig(good_number)
                if !result.nil? && result > 0
                    solution = solutions.dig(indice_name(result, good_number))
                    if solution.nil?
                        solutions[indice_name(array[i], good_number)] = [array[i], good_number]
                        hash[good_number] -= 1
                    end
                end
                i = i + 1
            end
            # puts hash
            return solutions
        end

        def indice_name(a, b)
            min = a > b ? b : a
            max = a > b ? a : b 
            "#{min}:#{max}"
        end
    end
end