class Candidate < ApplicationRecord
  belongs_to :talent_advocate
  has_many :job_offers
end
