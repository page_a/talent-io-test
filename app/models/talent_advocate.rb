class TalentAdvocate < ApplicationRecord
    has_many :candidates
    scope :best_advocates, -> {
        joins(:candidates).group("talent_advocate_id").
        order("sum(job_offers_count) DESC")
    }
end
