class Deal < ApplicationRecord
    def get_euro_amount
        context = ::Services::Fixerio::Convert.call(amount: self.amount, from: self.currency, to: 'EUR', date: self.created_at.strftime('%Y-%m-%d'))
        if context.success?
            return context.converted_result
        else
            return 0
        end
    end

    def self.global_revenue(period, date)
        @deals = Deal.send("by_#{period}", date)
        total = 0
        @deals.each do |deal|
            puts deal.get_euro_amount
            total += deal.get_euro_amount
        end
        total
    end

    private

    def self.by_year(date)
        beginning_of_year = date.beginning_of_year
        end_of_year = date.end_of_year
        Deal.where(created_at: beginning_of_year..end_of_year)
    end

    def self.by_month(date)
        beginning_of_month = date.beginning_of_month
        end_of_the_month = date.end_of_month
        Deal.where(created_at: beginning_of_month..end_of_the_month)
    end

    def self.by_week(date)
        beginning_of_week = date.beginning_of_week
        end_of_week = date.end_of_week
        Deal.where(created_at: beginning_of_week..end_of_week)
    end
end
